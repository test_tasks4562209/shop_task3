package products;

import java.io.*;
import java.util.*;

/**
 * Класс, представляющий склад с товарами.
 */
public class Warehouse implements Serializable {

    private String location;

    private final ArrayList<Product> products = new ArrayList<>();

    /**
     * Конструктор для создания объекта Warehouse с указанием местоположения.
     *
     * @param location Местоположение склада.
     */
    public Warehouse(String location) {

        this.location = location;

    }

    /**
     * Конструктор для создания объекта Warehouse с указанием местоположения и загрузкой данных из файла.
     *
     * @param filename Имя файла с данными о продуктах.
     * @param location Местоположение склада.
     */
    public Warehouse(String filename, String location) {

        readAsText(filename);
        this.location = location;

    }

    /**
     * Возвращает местоположение склада.
     *
     * @return Местоположение склада.
     */
    public String getLocation() {
        return location;
    }

    /**
     * Устанавливает новое местоположение склада.
     *
     * @param location Новое местоположение склада.
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * Добавляет продукт на склад.
     *
     * @param p Добавляемый продукт.
     */
    public void addProduct(Product p) {
        products.add(p);
    }


    /**
     * Удаляет продукт со склада.
     *
     * @param p Удаляемый продукт.
     */
    public void removeProduct(Product p) {
        for (Product temp : products) {
            if (temp.getName().equals(p.getName())) {
                products.remove(temp);
                break;
            }
        }

    }

    /**
     * Находит продукт по его названию.
     *
     * @param ref Название продукта.
     * @return Объект Product, если продукт найден, иначе null.
     */
    public Product findProduct(String ref)
    {
        for (Product temp: products)
        {
            if (temp.getName().equals(ref))
            {
                return temp;
            }
        }
        return null;
    }

    /**
     * Проверяет, есть ли продукт на складе по его названию.
     *
     * @param s Название продукта.
     * @return true, если продукт на складе, иначе false.
     */
    public boolean hasProduct(String s)
    {
        Product pd = findProduct(s);
        return pd != null;
    }


    /**
     * Возвращает информацию о продуктах на складе в виде строки.
     *
     * @return Информация о продуктах на складе.
     */
    public String getProducts() {

        String s = "Все Продукты: " + "\n";
        if(!products.isEmpty()) {
            for (Product sd : products) {s = s + sd.toString(); } }
        else { s = s + " Склад пуст "; }
        return s;
    }

    /**
     * Возвращает количество продукта по его идентификатору.
     *
     * @param productId Идентификатор продукта.
     * @return Количество продукта.
     */
    public int getProductQuantity(int productId) {
        for (Product product : products) {
            if (product.getId() == productId) {
                return product.getQuantity();
            }
        }
        return 0;
    }

    /**
     * Покупает продукт с указанным идентификатором и количеством.
     *
     * @param productId Идентификатор продукта.
     * @param quantity  Количество для покупки.
     * @return Сообщение о результате покупки.
     */
    public String buyProduct(int productId, int quantity) {
        String s = "";
        for (Product product : products) {
            if (product.getId() == productId) {
                if (product.getQuantity() >= quantity) {
                    product.setQuantity(product.getQuantity() - quantity);
                    //return
                    s = s + "Покупка прошла успешно " + "\n" ;
                } else {
                    //return
                    s = s + "Покупка отменена " + "\n" ;
                }
            }
        }
        return s;
    }

    /**
     * Сохраняет информацию о складе в файл.
     *
     * @param filename Имя файла для сохранения данных.
     */
    public void saveToFile(String filename) {
        try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(filename))) {
            outputStream.writeObject(this);
            System.out.println("Информация о складе сохранена в " + filename + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Загружает информацию о складе из файла.
     *
     * @param filename Имя файла с данными о складе.
     * @return Восстановленный объект Warehouse.
     */
    public Warehouse loadFromFile(String filename) {
        try (ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(filename))) {
            System.out.println("Информация о складе загружена из " + filename + "\n" );
            return (Warehouse)(inputStream.readObject());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }


    /**
     * Читает данные о продуктах из текстового файла и добавляет их на склад.
     *
     * @param filename Имя текстового файла с данными о продуктах.
     */
    public void readAsText(String filename) {
        try (Scanner scanner = new Scanner(new File(filename))) {

            while (scanner.hasNextLine()) {

                String[] tokens = scanner.nextLine().split(",");

                int product_Id = Integer.parseInt(tokens[0]);
                double price = Double.parseDouble(tokens[2]);
                int quantity = Integer.parseInt(tokens[3]);

                products.add(new Product(product_Id, tokens[1], price, quantity));


            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    /**
     * Возвращает строковое представление объекта Warehouse.
     *
     * @return Строковое представление объекта Warehouse.
     */
    public String toString() {
        return "Warehouse at " + location + "\n" + getProducts() + "\n";
    }
}

