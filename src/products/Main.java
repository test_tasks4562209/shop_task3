package products;

/**
 * Класс для запуска программы и демонстрации работы склада.
 */
public class Main {

    /**
     * Точка входа в программу.
     *
     * @param args Аргументы командной строки.
     */
    public static void main(String[] args) {

        //Warehouse oldwarehouse = new Warehouse( "Montaine Street");
        //System.out.println(oldwarehouse.getProducts());

        //Некоторые продукты добавляются в библиотеку из текстового файла при создании объекта склада
        Warehouse mywarehouse = new Warehouse("new_supplies.txt","Peter's Street");

        //System.out.println(mywarehouse.getProducts());

        mywarehouse.addProduct(new Product(1000, "Coffee", 4.98, 25));
        mywarehouse.addProduct(new Product(1001, "Eggs", 6.99, 36));

        System.out.println(" До покупки: \n" + mywarehouse);

        //Покупка
        int coffeeQuantity = 5; int mintQuantity = 10;
        System.out.println(mywarehouse.buyProduct(1000, coffeeQuantity));
        System.out.println(mywarehouse.buyProduct(2001, mintQuantity));

        System.out.println("После покупки: \n" + mywarehouse);

        //Сериализация
        mywarehouse.saveToFile("warehouse1.txt");

        //Десериализация
        Warehouse restoredWarehouse = mywarehouse.loadFromFile("warehouse1.txt");

        System.out.println("После загрузки:\n" + restoredWarehouse);


    }   }