package products;

import java.io.*;


/**
 * Класс, представляющий продукт.
 */
public class Product implements Serializable {

    private final  int id;
    private final String  name;
    private double price;
    private int quantity;

    /**
     * Конструктор для создания объекта Product.
     *
     * @param id       Уникальный идентификатор продукта.
     * @param name     Название продукта.
     * @param price    Цена продукта.
     * @param quantity Количество продукта.
     */
    public Product(int id, String name, double price, int quantity) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }

    /**
     * Возвращает уникальный идентификатор продукта.
     *
     * @return Уникальный идентификатор продукта.
     */
    public int getId() {
        return id;
    }

    /**
     * Возвращает название продукта.
     *
     * @return Название продукта.
     */
    public String getName() {
        return name;
    }

    /**
     * Возвращает цену продукта.
     *
     * @return Цена продукта.
     */
    public double getPrice() {
        return price;
    }

    /**
     * Устанавливает цену продукта.
     *
     * @param price Новая цена продукта.
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * Возвращает количество продукта.
     *
     * @return Количество продукта.
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Устанавливает количество продукта.
     *
     * @param quantity Новое количество продукта.
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * Возвращает строковое представление информации о продукте, уникальный идентификатор, название, цену и количество на складе.
     *
     * @return Строковое представление продукта.
     */
    public String toString() {
        return "Продукт ID: " + id + " -Название: " + name  + " Цена: " + price + " Количество: " + quantity + "\n";
    }
}



